import { FC, ReactElement } from "react";
import { Provider } from "react-redux";

/** Цепочка провайдеров приложения */

interface IAppProvidersProps {
  store: any; // TODO: Типизировать Store после его добавления
  children: ReactElement | ReactElement[];
}

const AppProviders = ({ store, children }: IAppProvidersProps) => {
  return <Provider store={store}>{children}</Provider>; // TODO: Добавить необходимые приложению провайдеры
};

interface IRootLayoutProps {
  store: any; // TODO: Типизировать Store после его добавления
  children: ReactElement | ReactElement[];
  // TODO: определить новые поля, в зависимости от потребностей лейаута
}

/** Компонент - лейаут приложения. Дорабатывается по мере необходимости */

export const RootLayoutComponent: FC<IRootLayoutProps> = ({
  store,
  children,
}) => {
  return <AppProviders store={store}>{children}</AppProviders>;
};
