import "../styles/globals.css";
import type { AppProps } from "next/app";
import { RootLayoutComponent } from "../components/RootLayoutComponent";
import { setupStore } from "../store";

const store = setupStore();

/** Корень приложения */

const App = ({ Component, pageProps }: AppProps) => {
  return (
    <RootLayoutComponent store={store}>
      <Component {...pageProps} />
    </RootLayoutComponent>
  );
};

export default App;
