import type { NextApiRequest, NextApiResponse } from "next";

/** Пример API на NextJS */

type Data = {
  name: string;
};

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  res.status(200).json({ name: "Ivan Baturin" });
}
