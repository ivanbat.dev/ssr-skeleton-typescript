import { Html, Head, Main, NextScript } from "next/document";
import { FC } from "react";

/** SEO структура приложения */

const Document: FC = () => {
  return (
    <Html lang="ru">
      <Head />
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
};

export default Document;
