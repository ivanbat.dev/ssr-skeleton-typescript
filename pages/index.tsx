import { FC } from "react";

/** Индексная страница приложения */

const Home: FC = () => {
  return (
    <div>
      <h1>Blank SSR (Next) Project</h1>
    </div>
  );
};

export default Home;
